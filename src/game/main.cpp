
#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

#include <iostream>

#if _WIN32
#include <Windows.h>
#endif

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );
	


#if 0
	//trying to create our own echo test by sending out UDP packets to a server and then getting
	//a ping back then cleaning everything up
	SocketUtil::StaticInit(); //initializing the socket library

	UDPSocketPtr myPtr; //creating my own socket pointer
	myPtr = nullptr; //not sure why
	myPtr = SocketUtil::CreateUDPSocket(SocketAddressFamily::INET); //I think this actually sets the UDP socket up 

	myPtr->SetNonBlockingMode(true); // this sets the string to not wait and send data when it receives it

	const SocketAddressPtr sockAddr = SocketAddressFactory::CreateIPv4FromString("127.0.0.1:7"); //This is the address I want to send to

	string sendString("Hello World! I do not understand what I am doing but I will get there"); //this is the string I want to send 

	int writeCount = myPtr->SendTo(static_cast<const void*>(sendString.c_str()), sendString.length() + 1, *sockAddr);//Don't understand the name writeCount
	//but the structure is grab my string i want to send, get the correct length and then where I want to send it to.

	const int bufferMax = 128;//max size of the buffer
	char buffer[bufferMax];//create a buffer array that is the correct size

	SocketAddressPtr otherAddr(new SocketAddress()); //create a new address to receive
	int result = 70;
	do {
		result = myPtr->ReceiveFrom(static_cast<void*>(buffer), bufferMax, *otherAddr);//what do I expect to receive, what will it's size be and where am I reading it from, i think?
	} while (result < 70);
	 


	buffer[result] = '\0'; //give the read count and ensure the end is a null terminating character
	string output(buffer); //put that info in a string

	if (result > 0) {
		std::cout << "Received: " << output << std::endl;
		//if it worked display it
	}

	myPtr.reset(); //reset the socket to avoid problems

	SocketUtil::CleanUp(); //clean up after myself
	
#endif
	//TCP Echo Test
#define ECHO_TCP "127.0.0.1:7"
	//set up the sockets and TCP addresses  
	SocketUtil::StaticInit();

	TCPSocketPtr server;
	TCPSocketPtr client;

	server = nullptr;
	client = nullptr;

	server = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);
	client = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);


	//Bind the socket

	const SocketAddressPtr  mySocketAddress = SocketAddressFactory::CreateIPv4FromString("0.0.0.0:54321");
	
	int serverReturnValue = server->Bind(*mySocketAddress);

	const SocketAddressPtr echoTCPAddress = SocketAddressFactory::CreateIPv4FromString(ECHO_TCP);

	int clientReturnValue = client->Connect(*echoTCPAddress);

	//string I want to send 

	string radnor("Radnor is a water bottle company");

	//send the string

	int sendOverNet = client->Send(static_cast<const void*> (radnor.c_str()), radnor.length() + 1);

	//echo now sends it back

	//create a buffer to store the returned string
	const int bufferMaxSize = 128;
	char buffer[bufferMaxSize];

	//receive the string back
	int receiveOverNet = client->Receive(static_cast<void*>(buffer), bufferMaxSize);

	//show me the string output
	string returnedString(buffer);


	//clean everything up
 	server.reset();
	client.reset();
	SocketUtil::CleanUp();

}

#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
